/**
 * Created by 43657389 on 30/08/2017.
 */
import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import java.awt.*;
import java.util.List;
import java.util.Optional;

public class Wolf extends Character {


    public Wolf(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.RED);

    }


}
